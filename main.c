/**
* @file main.c
* @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
* @version 1.0
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* http://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* Simple AES file encryptor. Uses http://gladman.plushost.co.uk/oldsite/AES. 
* See aes_lib/aes.txt for more information
*/

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <ctype.h>

#include "aes_lib/aes.h"
#include "aes_lib/aesopt.h"

/**
 * Default 128-bit key
 */
#define DEFAULT_AES_KEY "DEADC0DEBEE15559"

/**
 * Size of the encryption block. Should be 16 @see AES_BLOCK_SIZE in aes.h
 */
#define AES_BLOCK_LEN AES_BLOCK_SIZE

/**
 * Show the help message
 * @param argv0 - pointer to program name (argv[0])
 */
static void help_message(const char *argv0);

/**
 * Encrypt file
 * @param infile - pointer to input stream
 * @param outfile - pointer to output stream
 * @param key - AES key to encrypt
 * @param key_length - length of key (16, 24, 32 bytes or 128, 192, 256 in bits)
 * @return EXIT_SUCCESS if encryption succeed, EXIT_FAILURE otherwise
 */
static int encrypt_file(FILE *infile, FILE *outfile, const uint8_t * const key, size_t key_length);

/**
 * Decrypt file
 * @param infile - pointer to input stream
 * @param outfile - pointer to output stream
 * @param key - AES key to encrypt
 * @param key_length - length of key (16, 24, 32 bytes or 128, 192, 256 in bits)
 * @return EXIT_SUCCESS if encryption succeed, EXIT_FAILURE otherwise
 */
static int decrypt_file(FILE *infile, FILE *outfile, const uint8_t * const key, size_t key_length);

/**
 * Check if all symbols in a given string are HEX symnols and convert symbols to integers
 * @param in - input string with HEX symbols
 * @param out - output array of integers converted from symbols
 * @return if all symbols are hex
 */
static int convert_key_hex(const char *in, uint8_t *out);

int main(int argc, char *argv[])
{
    char *in_filename = NULL;
    char *out_filename = NULL;
    uint8_t key[32] = {0};
    size_t key_length = 0;
    uint_fast8_t decrypt_input_file = 0, custom_key = 0;

    int opt;
    while((opt = getopt(argc, argv, "o:k:i:hd")) != -1)
    {
        switch(opt)
        {
        case 'o':
            out_filename = (char *)malloc(strlen(optarg) + 1u);
            if(NULL == out_filename)
            {
                fprintf(stderr, "Cannot allocate memory for output filename: %s\n", strerror(errno));
                return EXIT_FAILURE;
            }
            strcpy(out_filename, optarg);
            break;
        case 'i':
            in_filename = (char *)malloc(strlen(optarg) + 1u);
            if(NULL == in_filename)
            {
                fprintf(stderr, "Cannot allocate memory for input filename: %s\n", strerror(errno));
                return EXIT_FAILURE;
            }
            strcpy(in_filename, optarg);
            break;
        case 'k':
            if(strlen(optarg) != 16u && strlen(optarg) != 24u && strlen(optarg) != 32u)
            {
                fprintf(stderr, "Error: the key must be 128, 192 or 256 bits length\n");
                return EXIT_FAILURE;
            }
            if(!convert_key_hex(optarg, key))
            {
                fprintf(stderr, "Error: the key must contain only HEX characters\n");
                return EXIT_FAILURE;
            }
            custom_key = 1u;
            key_length = strlen(optarg);
            break;
        case 'd':
            decrypt_input_file = 1u;
            break;
        case 'h':
        case ':':
        default:
            help_message(argv[0]);
            return EXIT_SUCCESS;
        }
    }

    if(NULL == in_filename)
    {
        fprintf(stderr, "Error: input filename is required\n");
        help_message(argv[0]);
        return EXIT_FAILURE;
    }
    if(NULL == out_filename)
    {
        fprintf(stderr, "Error: output filename is required\n");
        help_message(argv[0]);
        return EXIT_FAILURE;
    }
    if(!custom_key)
    {
        convert_key_hex(DEFAULT_AES_KEY, key);
        key_length = strlen(DEFAULT_AES_KEY);
    }

    FILE *infile = fopen(in_filename, "rb");
    if(NULL == infile)
    {
        fprintf(stderr, "Error: cannot open input file: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }
    FILE *outfile = fopen(out_filename, "wb");
    if(NULL == outfile)
    {
        fprintf(stderr, "Error: cannot open output file: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    aes_init();
    int res;
    if(decrypt_input_file)
    {
        res = decrypt_file(infile, outfile, key, key_length);
    }
    else
    {
        res = encrypt_file(infile, outfile, key, key_length);
    }
    fclose(outfile);
    fclose(infile);

    if(EXIT_SUCCESS == res)
    {
        fprintf(stdout, "AES %s file \"%s\" successfully written\n", decrypt_input_file ? "decrypted" : "encrypted", out_filename);
    }
    else
    {
        fprintf(stderr, "Error %s file\n", decrypt_input_file ? "decrypting" : "encrypting");
    }

    return res;
}

static void help_message(const char *argv0)
{
    fprintf(stdout, "Binary AES encryptor. Uses AES library http://gladman.plushost.co.uk/oldsite/AES\n"
                    "Usage:\n\t%s OPTIONS ARGUMENTS\nAvailable OTIONS and ARGUMENTS:\n"
                    "\t-o\toutput filename\n"
                    "\t-i\tinput filename\n"
                    "\t-k\tuse given AES key instead of default (can bee 128, 192 or 256 bit long, only hex characters)\n"
                    "\t-d\tdecrypt given input file instead (output file and key options remain)\n"
                    "\t-h\tshow this message\n"
                    "Example:\n\t%s -o image.bin -i build/firmware.bin\n", argv0, argv0);
}

static int convert_key_hex(const char *in, uint8_t *out)
{
    size_t i = 0, by = 0;
    while(i < 64u && *in)        // the maximum key length is 32 bytes and
    {                           // hence at most 64 hexadecimal digits
        char ch = toupper(*in++);    // process a hexadecimal digit
        if(ch >= '0' && ch <= '9')
        {
            by = (by << 4u) + ch - '0';
        }
        else if(ch >= 'A' && ch <= 'F')
        {
            by = (by << 4u) + ch - 'A' + 10u;
        }
        else                    // error if not hexadecimal
        {
            return 0;
        }
        // store a key byte for each pair of hexadecimal digits
        if(i++ & 1u)
        {
            out[i / 2u - 1u] = by & 0xFF;
        }
    }
    return 1u;
}

static void fillrand(uint8_t *buf, const size_t len)
{
    //  A Pseudo Random Number Generator (PRNG) used for the
    //  Initialisation Vector. The PRNG is George Marsaglia's
    //  Multiply-With-Carry (MWC) PRNG that concatenates two
    //  16-bit MWC generators:
    //      x(n)=36969 * x(n-1) + carry mod 2^16
    //      y(n)=18000 * y(n-1) + carry mod 2^16
    //  to produce a combined PRNG with a period of about 2^60.
    //  The Pentium cycle counter is used to initialise it. This
    //  is crude but the IV does not really need to be secret.
#define RAND(a,b) (((a = 36969 * (a & 65535) + (a >> 16)) << 16) + \
                        (b = 18000 * (b & 65535) + (b >> 16))  )

    static uint32_t a[2], mt = 1u, count = 4u;
    static uint32_t r[4];

    if(mt)
    {
        mt = 0;
        *(uint64_t *)a = time(NULL);
    }

    for(size_t i = 0; i < len; ++i)
    {
        if(count == 4)
        {
            *(uint32_t *)r = RAND(a[0], a[1]);
            count = 0;
        }
        buf[i] = r[count++];
    }
#undef RAND
}

static int encrypt_file(FILE *infile, FILE *outfile, const uint8_t * const key, size_t key_length)
{
    aes_encrypt_ctx ctx[1];

    if(EXIT_SUCCESS != aes_encrypt_key(key, key_length, ctx))
    {
        fprintf(stderr, "Error: cannot set aes encrypt key\n");
        return EXIT_FAILURE;
    }

    uint8_t dbuf[3 * AES_BLOCK_LEN];
    size_t read_len = 0, write_len = AES_BLOCK_LEN;

    // When ciphertext stealing is used, we three ciphertext blocks so
    // we use a buffer that is three times the block length.  The buffer
    // pointers b1, b2 and b3 point to the buffer positions of three
    // ciphertext blocks, b3 being the most recent and b1 being the
    // oldest. We start with the IV in b1 and the block to be decrypted
    // in b2.

    // set a random IV
    fillrand(dbuf, AES_BLOCK_LEN);
    // read the first file block
    read_len = fread(dbuf + AES_BLOCK_LEN, 1, AES_BLOCK_LEN, infile);
    if(read_len < AES_BLOCK_LEN)
    {   // if the file length is less than one block
        // xor the file bytes with the IV bytes
        for(size_t i = 0; i < read_len; ++i)
        {
            dbuf[i + AES_BLOCK_LEN] ^= dbuf[i];
        }
        // encrypt the top 16 bytes of the buffer
        aes_encrypt(dbuf + read_len, dbuf + read_len, ctx);
        read_len += AES_BLOCK_LEN;
        // write the IV and the encrypted file bytes
        if(fwrite(dbuf, 1, read_len, outfile) != read_len)
        {
            fprintf(stderr, "Error: cannot write data to file: %s\n", strerror(errno));
            return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
    }
    else // if the file length is more 16 bytes
    {
        uint8_t *b1 = dbuf, *b2 = b1 + AES_BLOCK_LEN, *b3 = b2 + AES_BLOCK_LEN, *bt;
        // write the IV
        if(fwrite(dbuf, 1, AES_BLOCK_LEN, outfile) != AES_BLOCK_LEN)
        {
            fprintf(stderr, "Error: cannot write data to file: %s\n", strerror(errno));
            return EXIT_FAILURE;
        }

        while(1)
        {
            // read the next block to see if ciphertext stealing is needed
            read_len = fread(b3, 1, AES_BLOCK_LEN, infile);
            // do CBC chaining prior to encryption for current block (in b2)
            for(size_t i = 0; i < AES_BLOCK_LEN; ++i)
            {
                b1[i] ^= b2[i];
            }
            // encrypt the block (now in b1)
            aes_encrypt(b1, b1, ctx);
            if(read_len != 0 && read_len != AES_BLOCK_LEN) // use ciphertext stealing
            {
                // set the length of the last block
                write_len = read_len;
                // xor ciphertext into last block
                for(size_t i = 0; i < read_len; ++i)
                {
                    b3[i] ^= b1[i];
                }
                // move 'stolen' ciphertext into last block
                for(size_t i = read_len; i < AES_BLOCK_LEN; ++i)
                {
                    b3[i] = b1[i];
                }
                // encrypt this block
                aes_encrypt(b3, b3, ctx);
                // and write it as the second to last encrypted block
                if(fwrite(b3, 1, AES_BLOCK_LEN, outfile) != AES_BLOCK_LEN)
                {
                    fprintf(stderr, "Error: cannot write data to file: %s\n", strerror(errno));
                    return EXIT_FAILURE;
                }
            }
            // write the encrypted block
            if(fwrite(b1, 1, write_len, outfile) != write_len)
            {
                fprintf(stderr, "Error: cannot write data to file: %s\n", strerror(errno));
                return EXIT_FAILURE;
            }
            if(read_len != AES_BLOCK_LEN)
            {
                return EXIT_SUCCESS;
            }
            // advance the buffer pointers
            bt = b3, b3 = b2, b2 = b1, b1 = bt;
        }
    }
    return EXIT_SUCCESS;
}

static int decrypt_file(FILE *infile, FILE *outfile, const uint8_t * const key, size_t key_length)
{
    aes_decrypt_ctx ctx[1];

    if(EXIT_SUCCESS != aes_decrypt_key(key, key_length, ctx))
    {
        fprintf(stderr, "Error: cannot set aes decrypt key\n");
        return EXIT_FAILURE;
    }

    uint8_t dbuf[3 * AES_BLOCK_LEN], buf[AES_BLOCK_LEN];
    size_t read_len = 0, write_len = AES_BLOCK_LEN;

    // When ciphertext stealing is used, we three ciphertext blocks so
    // we use a buffer that is three times the block length.  The buffer
    // pointers b1, b2 and b3 point to the buffer positions of three
    // ciphertext blocks, b3 being the most recent and b1 being the
    // oldest. We start with the IV in b1 and the block to be decrypted
    // in b2.

    read_len = fread(dbuf, 1, 2 * AES_BLOCK_LEN, infile);
    if(read_len < 2 * AES_BLOCK_LEN) // the original file is less than one block in length
    {
        read_len -= AES_BLOCK_LEN;
        // decrypt from position read_len to position len + BLOCK_LEN
        aes_decrypt(dbuf + read_len, dbuf + read_len, ctx);
        // undo the CBC chaining
        for(size_t i = 0; i < read_len; ++i)
        {
            dbuf[i] ^= dbuf[i + AES_BLOCK_LEN];
        }
        // output the decrypted bytes
        if(fwrite(dbuf, 1, read_len, outfile) != read_len)
        {
            fprintf(stderr, "Error: cannot write data to file: %s\n", strerror(errno));
            return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
    }
    else
    {
        uint8_t *b1 = dbuf, *b2 = b1 + AES_BLOCK_LEN, *b3 = b2 + AES_BLOCK_LEN, *bt;
        while(1) // while some ciphertext remains, prepare to decrypt block b2
        {
            // read in the next block to see if ciphertext stealing is needed
            read_len = fread(b3, 1, AES_BLOCK_LEN, infile);

            // decrypt the b2 block
            aes_decrypt(b2, buf, ctx);

            if(read_len == 0 || read_len == AES_BLOCK_LEN) // no ciphertext stealing
            {
                // unchain CBC using the previous ciphertext block in b1
                for(size_t i = 0; i < AES_BLOCK_LEN; ++i)
                {
                    buf[i] ^= b1[i];
                }
            }
            else // partial last block - use ciphertext stealing
            {
                write_len = read_len;
                // produce last 'read_len' bytes of plaintext by xoring with
                // the lowest 'read_len' bytes of next block b3 - C[N-1]
                for(size_t i = 0; i < read_len; ++i)
                {
                    buf[i] ^= b3[i];
                }
                // reconstruct the C[N-1] block in b3 by adding in the
                // last (AES_BLOCK_LEN - read_len) bytes of C[N-2] in b2
                for(size_t i = read_len; i < AES_BLOCK_LEN; ++i)
                {
                    b3[i] = buf[i];
                }

                // decrypt the C[N-1] block in b3
                aes_decrypt(b3, b3, ctx);

                // produce the last but one plaintext block by xoring with
                // the last but two ciphertext block
                for(size_t i = 0; i < AES_BLOCK_LEN; ++i)
                {
                    b3[i] ^= b1[i];
                }

                // write decrypted plaintext blocks
                if(fwrite(b3, 1, AES_BLOCK_LEN, outfile) != AES_BLOCK_LEN)
                {
                    fprintf(stderr, "Error: cannot write data to file: %s\n", strerror(errno));
                    return EXIT_FAILURE;
                }
            }
            // write the decrypted plaintext block
            if(fwrite(buf, 1, write_len, outfile) != write_len)
            {
                fprintf(stderr, "Error: cannot write data to file: %s\n", strerror(errno));
                return EXIT_FAILURE;
            }

            if(read_len != AES_BLOCK_LEN)
            {
                return EXIT_SUCCESS;
            }
            // advance the buffer pointers
            bt = b1, b1 = b2, b2 = b3, b3 = bt;
        }
    }
    return EXIT_SUCCESS;
}
