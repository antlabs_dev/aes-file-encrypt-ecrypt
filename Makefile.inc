TOOLCHAIN_PATH = 

CC = $(TOOLCHAIN_PATH)gcc
CFLAGS = -g -Wall -std=c99
LD = $(TOOLCHAIN_PATH)-gcc
LDFLAGS = -Laes_lib/
AR = $(TOOLCHAIN_PATH)ar
ARFLAGS = 
RANLIB = $(TOOLCHAIN_PATH)ranlib
OBJCOPY = $(TOOLCHAIN_PATH)objcopy
ECHO = echo
RM = rm
XARGS = xargs
FIND = find
SHELL = /bin/sh
MAKE = make
MFLAGS = 

.SILENT :
