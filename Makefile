include Makefile.inc

BINARY = aes_crypt_binary
SOURCES = $(wildcard *.c)
OBJLIBS = aes_lib/libaescrypt.a
OBJS = $(SOURCES:.c=.o)

all: $(BINARY)

$(BINARY): $(OBJLIBS) $(OBJS)
	$(LD) -o $(BINARY) $(LDFLAGS) $(OBJS) $(OBJLIBS)

aes_lib/libaescrypt.a: force_look
	cd aes_lib; $(MAKE) $(MFLAGS)

clean:
	$(RM) *.o $(BINARY) $(BINARY).exe
	cd aes_lib; $(MAKE) clean
	
install:
	$(INSTALL_COMMAND)

force_look:
	true
